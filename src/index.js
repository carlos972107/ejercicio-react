import React from 'react';
import { render } from 'react-dom';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from "@apollo/react-components";
import App from './App';
import * as serviceWorker from './serviceWorker';

//iniciamos nuestra base de la url para realizar las peticiones graphql
const httpLink = createHttpLink({
  uri: "https://api-ejercicio-graphql.herokuapp.com/",//"https://",
  credentials: "same-origin",
});

// crea una instancia de memoria caché inmemory para el almacenamiento en caché de datos graphql
const cache = new InMemoryCache();

const authLink = setContext((_, { headers }) => {
  // obtenemos el token de authentication si existe en nuestro localStorage
  const token = localStorage.getItem('token');
  // devolvemos los encabezados al contexto para que httpLink pueda leerlos
  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : "",
    }
  }
});

// inicilizamos apollo client con apollo link y intaciamos la cache
const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache,
  onError: (e) => { console.log(e) },
});

render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
