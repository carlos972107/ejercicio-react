import React, { Component } from 'react';
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Login, Register, List, Clientes } from './pages';
import history from './history';

class App extends Component {
  render(){
    return (
      <Router history={history}>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/Login" />}/>
          <Route path="/Login" exact component={Login} />
          <Route path="/Register" exact component={Register} />
          <Route path="/List" exact component={List} />
          <Route path="/Clientes/:id?" exact component={Clientes} />
        </Switch>
      </Router>
    )
  }
}

export default App;
