import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Mutation } from '@apollo/react-components';
import gql from 'graphql-tag';

const DELETE = gql`
    mutation deleteCliente($id: String!){
        deleteCliente(id: $id){
            id
            name
            lastname
        }
    }
`;

export default class ListCliente extends Component{
    constructor(props){
        super(props)
        this.state = {
            clientes: []
        }
    }

    componentDidMount(){
        this.setState({
            clientes: this.props.data.clientes
        })
    }

    render(){
        return(
            <Mutation mutation={DELETE}>
                {deleteId => (
                <table className="table table-sm table-bordered">
                    <thead>
                        <tr className="text-center">
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Correo</th>
                            <th>Telefono</th>
                            <th>Residencia</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.clientes.map(cliente => (
                                <tr key={cliente.id}>
                                    <td>{cliente.id}</td>
                                    <td>{cliente.name}</td>
                                    <td>{cliente.lastname}</td>
                                    <td>{cliente.email}</td>
                                    <td>{cliente.phone}</td>
                                    <td>{cliente.locale}</td>
                                    <td>
                                        <Link to={`/Clientes/${cliente.id}`} className="btn btn-primary btn-sm mr-1">Modificar</Link>
                                        <Link className="btn btn-danger btn-sm" onClick={async () =>{
                                            const {data} = await deleteId({variables: {id: cliente.id}});
                                            if(data.deleteCliente)
                                                alert('Eliminado correctamente');
                                        }}>Eliminar</Link>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            )}
            </Mutation>
        )
    }
}