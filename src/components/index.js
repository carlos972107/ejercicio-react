import FormLogin from './Form/formLogin';
import FormSignup from './Form/formSingup';
import ListCliente from './List/ListClientes';
import FormCliente from './Form/formCliente';
import Navbar from './Nav/Navbar';

export {
    FormLogin,
    FormSignup,
    ListCliente,
    FormCliente,
    Navbar
}