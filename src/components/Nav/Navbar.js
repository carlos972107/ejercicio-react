import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import history from '../../history';

export default class Navbar extends Component{
    handlerClick(){
        localStorage.clear();
        history.push('/Login');
    }
    render(){
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark static-top">
                <div className="container">
                    <Link className="navbar-brand" to="/List">Actividad</Link>
                    <div className="collapse navbar-collapse" id="navbarResponsive">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <span className="nav-link" onClick={this.handlerClick}>Cerrar sesion</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}