import React, { Component } from 'react';
import { Mutation } from '@apollo/react-components';
import gql from 'graphql-tag';
import history from '../../history';

const LOGIN = gql`
    mutation Login($email: String!, $password: String!){
        login(email: $email, password: $password)
    }
`;

export default class FormLogin extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            credential: false
        }
        this.handlerChange = this.handlerChange.bind(this)
    }

    handlerChange(e){
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }

    render(){
        const { email, password } = this.state
        return(
            <Mutation mutation={LOGIN} >
                { login => (
                <form 
                    onSubmit={ async e => {
                        e.preventDefault()
                        const { data } = await login({variables: {email, password}});
                        if(!data.login)
                            this.setState({credential: !this.state.credential})
                        else {
                            localStorage.setItem('token', data.login);
                            history.push('/List');
                        }
                }}>
                    <div className="form-group">
                        <label className="small mb-1" htmlFor="email">Correo</label>
                        <input className="form-control py-4" name="email" type="email" placeholder="Correo" value={email} onChange={this.handlerChange} required/>
                    </div>
                    <div className="form-group">
                        <label className="small mb-1" htmlFor="password">Contraseña</label>
                        <input className="form-control py-4" name="password" type="password" placeholder="Contraseña" value={password} onChange={this.handlerChange} required/>
                    </div>
                    <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                        <button className="btn btn-primary btn-block" type="submit">Iniciar sesion</button>
                    </div>
                    {this.state.credential &&
                    <div className="alert alert-danger">
                        <strong>Error!,</strong> credenciales invalidas.
                    </div>}
                </form>
                )}
            </Mutation>
        )
    }
}