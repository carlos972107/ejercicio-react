import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class FormCliente extends Component{
    render(){
        const { id, name, lastname, phone, email, locale } = this.props.data
        return(
            <form className="mt-5" {...this.props}>
                <div className="form-group">
                    <label className="small mb-1" htmlFor="id">Id</label>
                    <input className="form-control py-4" name="id" type="text" placeholder="id" defaultValue={id} onChange={this.props.handlerChange} readOnly/>
                </div>
                <div className="form-row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label className="small mb-1" htmlFor="lastname">Apellido</label>
                            <input className="form-control py-4" name="lastname" type="text" placeholder="Apellido" defaultValue={lastname} onChange={this.props.handlerChange} required/>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label className="small mb-1" htmlFor="name">Nombre</label>
                            <input className="form-control py-4" name="name" type="text" placeholder="Nombre" defaultValue={name} onChange={this.props.handlerChange} required/>
                        </div>
                    </div>
                </div>
                <div className="form-group">
                    <label className="small mb-1" htmlFor="email">Correo</label>
                    <input className="form-control py-4" name="email" type="email" placeholder="Correo electronico" defaultValue={email} onChange={this.props.handlerChange} required/>
                </div>
                <div className="form-row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label className="small mb-1" htmlFor="phone">Telefono</label>
                            <input className="form-control py-4" name="phone" type="text" placeholder="Telefono" defaultValue={phone} onChange={this.props.handlerChange} required/>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label className="small mb-1" htmlFor="locale">Residencia</label>
                            <input className="form-control py-4" name="locale" type="text" placeholder="Residencia" defaultValue={locale} onChange={this.props.handlerChange} />
                        </div>
                    </div>
                </div>
                <div className="form-group mt-4 mb-0">
                    <button className="btn btn-primary btn-block" type="submit">{this.props.options}</button>
                    <Link to="/List" className="btn btn-danger btn-block">Cancelar</Link>
                </div>
            </form>
            
        )
    }
}