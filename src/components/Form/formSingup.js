import React, { Component } from 'react';
import { Mutation } from '@apollo/react-components';
import gql from 'graphql-tag';

const CREATEUSER = gql`
    mutation Signup($username: String!, $email: String!, $password: String!){
        signup(username: $username, email: $email, password: $password)
    }
`;

export default class FormSignup extends Component{
    constructor(props){
        super(props)
        this.state = {
            username: '',
            email: '',
            password: '',
            confirm: false
        }
        this.handlerChange = this.handlerChange.bind(this)
    }
    handlerChange(e){
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }
    render(){
        const { username, email, password } = this.state
        return(
            <Mutation mutation={CREATEUSER}>
                {create => (
                <form 
                    onSubmit={async e => {
                        e.preventDefault()
                        const {data} = await create({variables: {username, email, password}});
                        if(data.signup)
                            alert('usuario creado');
                }}>
                    <div className="form-row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="small mb-1" htmlFor="username">Nombre de usuario</label>
                                <input className="form-control py-4" name="username" type="text" placeholder="Nombre de usuario" defaultValue={username} onChange={this.handlerChange} required/>
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="small mb-1" htmlFor="email">Correo</label>
                        <input className="form-control py-4" name="email" type="email" placeholder="Correo electronico" defaultValue={email} onChange={this.handlerChange} required/>
                    </div>
                    <div className="form-row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label className="small mb-1" htmlFor="password">Contraseña</label>
                                <input className="form-control py-4" name="password" type="password" placeholder="Contraseña" defaultValue={password} onChange={this.handlerChange} required/>
                            </div>
                        </div>
                    </div>
                    <div className="form-group mt-4 mb-0">
                        <button className="btn btn-primary btn-block" type="submit">Crear</button>
                    </div>
                </form>
                )}
            </Mutation>
        )
    }
}