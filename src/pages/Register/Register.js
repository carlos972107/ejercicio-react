import React, { Component } from 'react';
import { FormSignup } from '../../components';
import { Link } from 'react-router-dom';

export default class Register extends Component{
    render(){
        return(
            <main>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-7">
                            <div className="card mt-5">
                                <div className="card-header">
                                    <h3 className="text-center font-weight-light my-4">Crear una cuenta</h3>
                                </div>
                                <div className="card-body">
                                    <FormSignup />
                                </div>
                                <div className="card-footer text-center">
                                    <div className="small">
                                        <Link to="/Login">Regresar al inicio de sesion</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}