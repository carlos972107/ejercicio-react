import React, { Component } from 'react';
import { ListCliente } from '../../components';
import history from '../../history';
import { Navbar } from '../../components';
import { Query } from '@apollo/react-components';
import gql from 'graphql-tag';
import { Link } from 'react-router-dom';

const CLIENTES = gql`
    query{
        clientes{
            id
            name
            lastname
            email
            phone
            locale
        }
    }
`;

export default class List extends Component{
    componentDidMount(){
        if(!localStorage.getItem('token'))
            history.push('/Login');
    }
    render(){
        return(
            <div>
                <Navbar />
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-12 text-center">
                                <h1 className="mt-5">Listado Clientes</h1>
                                <Link to="/Clientes" className="btn btn-primary btn-sm float-right mb-3">Añadir</Link>
                                <Query query={CLIENTES}>
                                    {({loading, error, data}) => {
                                        if(loading) return(<p>Cargando...</p>);
                                        if(error) return `Error ${error}`;
                                        return <ListCliente data={data} />
                                    }}
                                </Query>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}