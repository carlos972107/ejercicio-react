import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FormLogin } from '../../components';

export default class Login extends Component{
    render(){
        return(
            <main>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-5">
                            <div className="card mt-5">
                                <div className="card-header">
                                    <h3 className="text-center font-weight-light my-4">Inicio de sesion</h3>
                                </div>
                                <div className="card-body">
                                    <FormLogin />
                                </div>
                                <div className="card-footer text-center">
                                    <div className="small">
                                        <Link to="/register">No tienes una cuenta? creemos una!</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}