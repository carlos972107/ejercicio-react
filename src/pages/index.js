import Login from './Login/Login';
import Register from './Register/Register';
import List from './List/List';
import Clientes from './Clientes/Clientes';

export {
    Login,
    Register,
    List,
    Clientes
};