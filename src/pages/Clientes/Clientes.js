import React, { Component } from 'react';
import { Navbar, FormCliente } from '../../components';
import history from '../../history';
import gql from 'graphql-tag';
import { Query, Mutation } from '@apollo/react-components';

const CREATE = gql`
    mutation createCliente($name: String!, $lastname: String!, $email: String!, $phone: String, $locale: String){
        createCliente(name: $name, lastname: $lastname, email: $email, phone: $phone, locale: $locale){
            id
            name
            lastname
            email
            phone
            locale
        }
    }
`;

const UPDATE = gql`
    mutation updateCliente($id: String!, $name: String!, $lastname: String!, $email: String!, $phone: String, $locale: String){
        updateCliente(id: $id, name: $name, lastname: $lastname, email: $email, phone: $phone, locale: $locale){
            id
            name
            lastname
            email
            phone
            locale
        }
    }
`;

const CLIENTE = gql`
    query cliente($id: String!){
        cliente(id: $id){
            id
            name
            lastname
            email
            phone
            locale
        }
    }
`;

export default class Clientes extends Component{
    constructor(props){
        super(props)
        this.state = {
            id: '',
            name: '',
            lastname: '',
            phone: '',
            email: '',
            locale: ''
        }
        this.handlerChange = this.handlerChange.bind(this)
    }

    componentDidMount(){
        let url = window.location.href.split('/')
        let id = url[url.length - 1]
        this.setState({
            id: id !== 'Clientes' ? id : ''
        })
        if(!localStorage.getItem('token'))
            history.push('/Login');
    }

    handlerChange(e){
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }

    resetValue(){
        this.setState({
            id: '',
            name: '',
            lastname: '',
            phone: '',
            email: '',
            locale: ''
        });
    }

    render(){
        const { id, name, lastname, phone, email, locale } = this.state
        return(
            <div>
                <Navbar />
                <div className="container">
                    <div className="row justify-content-center">
                        <Mutation 
                            mutation={this.state.id ? UPDATE : CREATE}
                        >
                        {mutation => (
                        !this.state.id ?
                            <FormCliente 
                                data={this.state}
                                options="Guardar"
                                onChange={this.handlerChange}
                                onSubmit={ async e => {
                                    e.preventDefault()
                                    let {data} = await mutation({variables: { name, lastname, phone, email, locale}})
                                    if(data.createCliente)
                                        alert("creado correctamente")
                                    this.resetValue()
                                }}
                             />
                        :
                            <Query query={CLIENTE} variables={{id: this.state.id}}>
                                {({loading, error, data})=>{
                                    if(loading) return(<p>Cargando...</p>)
                                    if(error) return `Error ${error}`;
                                    return(
                                        <FormCliente 
                                            data={data.cliente}
                                            options="Actualizar"
                                            onChange={this.handlerChange}
                                            onSubmit={ async e => {
                                                e.preventDefault()
                                                const {data} = await mutation({variables: { id, name, lastname, phone, email, locale}})
                                                if(data.updateCliente)
                                                    alert("modificado correctamente")
                                                this.resetValue()
                                            }} />
                                    )
                                }}
                            </Query>
                        )}
                        </Mutation>
                    </div>
                </div>
            </div>
        )
    }
}